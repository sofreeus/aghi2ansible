Clone aghi2ansible and GLADOS

```bash
mkdir -p ~/git
cd $_
git clone git@gitlab.com:sofreeus/aghi2ansible.git
git clone git@gitlab.com:sofreeus/glados.git
```

Use GLADOS to create a server named `ansible-controller`

```bash
cd ~/git/glados
# Add the OpenStack secret to a hidden file
vim .OS_APPLICATION_CREDENTIAL_SECRET
export OS_APPLICATION_CREDENTIAL_SECRET=CRL...rQw
./cx-openstack openstack server create --key-name GitLab-DLWillson-2024-11-11 --security-group fuga-node-sg --flavor s5.medium --network public --image "Fedora 40" ansible-controller
```

Get the Public IP address of the Ansible Controller

```bash
./cx-openstack openstack server list
```

Manually add an A record in Gandi DNS

`ansible-controller.sofree.us A 123.45.67.90`

Update admins and users in `host_vars/ansible-controller.sofree.us.yml`

Add Linux admins as `fedora`

```bash
./add-linux-admins.sh fedora ansible-controller.sofree.us ansible-controller.key
```

Update Linux admins and drop `fedora` as yourself.

```bash
ansible-playbook update-linux-admins.yml --extra-vars="target=ansible-controller.sofree.us"
```

Add Ansible, AWS CLI, and whatever else is needed.

```bash
ansible-playbook configure-ansible-controller.yml
```

Add a shallow clone of this repo to /etc/skel/

```bash
scp git-a-class ansible-controller.sofree.us:
ssh ansible-controller.sofree.us
chmod +x git-a-class
./git-a-class aghi2ansible
sudo cp -r sfs /etc/skel/
```

Update Linux users *last*.

```bash
ansible-playbook update-linux-users.yml --extra-vars="target=ansible-controller.sofree.us"
```

Verify that a user can `git pull`

```bash
sudo su - someuser
cd sfs/aghi2ansible/
git pull

# openstack server list?
```
