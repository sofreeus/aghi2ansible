# Ansible for Configuration Management

Get ready.

```bash
ssh ansible-controller.sofree.us
cd sfs... # tab twice
git pull
cd labs/02... # tab
```

Arm your session with OpenStack environment variables

```bash
source /tmp/orc
```

Note that OpenStack has been configured as an inventory source

```bash
cat ansible.cfg
cat openstack.yml
```

Ensure that you have some servers

```bash
ansible all --list
openstack server list
# If you *don't* see servers with your MacGuffin, go redo the IaC lab
```

"Ansible ping" one of your servers

```bash
# It's nice not to have to approve new keys, so
# Create an ssh config that accepts new keys
mkdir -p ~/.ssh/
echo "StrictHostKeyChecking accept-new" > ~/.ssh/config
# Ansible ping your server using your key and the default username
# replace alicorn with your MacGuffin
ansible alicorn-rojo -u debian -m ping
```

Add yourself and your partner to your machine

```bash
# replace 'dlwillson' and 'aayore' with your and your partner's GitLab usernames
vim add-linux-admins.yml
# add yourself and your partner to your machines as admins
# replace alicorn with your MacGuffin
ansible-playbook add-linux-admins.yml -u debian --limit 'alicorn-*'
# run the exact same expression again, have another discussion about idempotence
```

Run the add-linux-admins.yml playbook as yourself, i.e. remove the `-u debian`.
If that works, remove the debian user with:

```bash
# replace alicorn with your MacGuffin
ansible alicorn-* -m user -a "name=debian state=absent" --become
# run it again, grok idempotent
```

## Install a service, configure it, and start and enable it.

Let's build a website!

```bash
# get list of names of your servers
# replace alicorn with your MacGuffin
ansible all --list --limit=alicorn-*
# replace 'alicorn-rojo' with one of your machines
ansible-playbook webian.yml --limit 'alicorn-rojo'
```

Now, point curl or your web-browser at the public IP of that machine

```bash
# get the $PUBLIC_IP of the server you ran the webian.yml playbook on
openstack server list
# request the web page
curl $PUBLIC_IP
xdg-open $PUBLIC_IP
```

Edit the template. Notice that it uses some built-in Ansible variables.

Curious what variables are available? Try this:

```bash
ansible some-host -m setup
# Add something to the website template
# ansible_distribution_version or something equally interesting
vim index.html.j2
# Then, re-run the playbook. Note which tasks fire, and which do not
ansible-playbook webian.yml --limit 'alicorn-rojo'
# Finally, re-run curl or refresh your browser
```
