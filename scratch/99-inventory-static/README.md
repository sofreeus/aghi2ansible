# Static Inventory

Look at the example.

```bash
cd ~/sfs/aghi2ansible/labs/02-inventory-static/
cat ansible.cfg
cat hostlist
ansible all --list
ansible-inventory --graph
```

Create a new hostlist with group names you like better ([elves] and [dwarves]?
[blocks] and [tackles]?), and add the IP addresses of your actual machines.

If you didn't write down and don't *remember* the IP addresses of your machines,
go back to the Inventory lab and re-run that playbook.

```bash
vim hostlist
```

Try a few things and this time, check connectivity.

```bash
ansible all --list
ansible-inventory --graph
ansible all -m ping
ansible $SOME_GROUP -m command -a uptime
```
