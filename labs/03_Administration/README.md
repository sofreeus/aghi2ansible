# Ansible Ad Hoc Operations

The default module is 'command', so you can run all sorts of things without specifying a module

```bash
# Replace 'alicorn' with your MacGuffin
export TARGET='alicorn-*'
ansible $TARGET -a "getent passwd $USER"
ansible $TARGET -a "grep $USER /etc/passwd"
ansible $TARGET -a "id $USER"
```

To use chains or redirection, specify the 'shell' module, instead.

```bash
ansible $TARGET -m shell \
    -a "systemctl list-unit-files | grep -Ei '(apache|http)'"
```

Use the 'fetch' module to fetch all the sshd configs

```bash
ansible $TARGET -m fetch -a "src=/etc/ssh/sshd_config dest=~/fetched/" -b
ls -l ~/fetched/*/etc/ssh/sshd_config
ansible $TARGET -m fetch -a "src=/etc/ssh/sshd_config dest=~/fetched/" -b
```

Now, fetch all the /var/log/lastlogses, precious.

```bash
ansible $TARGET -m fetch -a "src=/var/log/lastlog dest=~/fetched" --become
ls -l ~/fetched/*/var/log/lastlog
ansible $TARGET -m fetch -a "src=/var/log/lastlog dest=~/fetched" --become
```

Notice that this one *never* goes stable. Hmm... I wonder why...

WOULD YOU LIKE TO KNOW
<blink>[MORE](https://docs.ansible.com/ansible/latest/command_guide/intro_adhoc.html)</blink>?

Imagine that your machines are load-balanced webservers. Can you write a playbook or an ad hoc expression to reboot them one at a time?
