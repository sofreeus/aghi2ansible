# Ansible for Infrastructure as Code

Get ready.

```bash
ssh ansible-controller.sofree.us
cd sfs... # tab twice
git pull
cd labs/01... # tab
```

Arm your session with OpenStack environment variables

```bash
source /tmp/orc
```

## Create your Key in OpenStack

Copy your own public key into the playbook and run it

```bash
ssh-add -L
# triple-click the output and Copy it
vim create-my-key.yml # or whichever editor you like
# change 'alicorn' to your favorite MacGuffin
# change the key value to what you copied
# add your key
ansible-playbook create-my-key.yml
# re-run the playbook and have a nice discussion about what 'idempotent' means
openstack key list
```

## Create Servers in OpenStack

Customize the playbook with your MacGuffin and run it

```bash
vim create-my-servers.yml
# change 'alicorn' to the same MacGuffin you used for your key name
ansible-playbook create-my-servers.yml
openstack server list
```
