# A Gentle Hands-on Intro 2 Ansible

![Ansible](ansible.png)

## Next delivery Friday 2024-11-22 online

## The SFS Rap

[wrapper slides](wrapper-slides.fodp)

---

## Discussion: What is Ansible?

Ansible is Configuration Management and general-purpose Automation

Compare shell, puppet, ansible, salt, chef

cf: https://jjasghar.github.io/blog/2015/12/20/chef-puppet-ansible-salt-rosetta-stone/

## [Check Requirements](labs/00_Get_Reqd)

- Web Browser
- GitLab.com account. Example: www.gitlab.com/dlwillson
- Secure shell client. Example: `ssh` from Bash or PowerShell
- Secure shell keypair. Create with `ssh-keygen`, if needed
- Secure shell keypair authorization.
  https://gitlab.com/-/user_settings/ssh_keys

## [Infrastructure as Code](labs/01_IaC_Infrastructure_as_Code/)

- Create a key in OpenStack
- Create servers in OpenStack

## [Configuration Management](labs/02_CM_Configuration_Management)

- Add yourself and your partner as admins
- Remove the default admin to avoid abuse of shared accounts
- Configure a Web Server
- Add a Simple Web Page

## [Ad Hoc Administration](labs/03_Administration)

## Discussion 1: What does "idempotent" mean?

What does it mean to say, "Good playbooks and modules are 'idempotent'?"

## ToDo

- Roles
- Collections / Galaxy
- Wordpress or Ghost Playbook
- Docker Compose:
    * Prometheus, Grafana, and Friends

## Classroom Setup

[Teacher Tools](teacher-tools/)
